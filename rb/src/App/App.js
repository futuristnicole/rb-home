import React from 'react';
import { Route, Switch } from 'react-router-dom';
// import logo from './logo.svg';
import Home from '../components/05-pages/Home'
import './App.css';

function App() {
  return (
    <Switch>
      <Route path="/"  component={Home} />
    </Switch>
    // <div className="App">
    //   <header className="App-header">
    //     <Home />
    //   </header>
    // </div>
  );
}

export default App;
